<?php

namespace Spark\Common\Descriptor;

/**
 * 
 *
 * @author Chris Harris
 * @version 1.0.0
 * @internal
 */
class UseDescriptor
{    
    /**
     * Indicates that the use statement represents a class name, interface name or namespace name.
     *
     * @var int
     */
    const IS_NAMESPACE = 1;
    
    /**
     * Indicates that the use statement represents a constant.
     *
     * @var int
     */
    const IS_CONSTANT = 2;
    
    /**
     * Indicates that the use statement represents a function.
     *
     * @var int
     */
    const IS_FUNCTION = 4;

    /**
     * The type of statement which defaults to a namespace.
     *
     * @var string
     */
    private $type = 1;
    
    /**
     * The fully qualified name.
     *
     * @var string
     */
    private $namespace;
    
    /**
     * The last part of the fully qualified name.
     *
     * @var string
     */
    private $name;
    
    /**
     * The alias associated with the use statement.
     *
     * @var string
     */
    private $alias;

    /**
     * Constructs a new {@link UseDescriptor} object.
     *
     * @param string|null $stmt a use statement.
     */
    public function __construct($stmt = null)
    {
    
    }

    /**
     * Returns true if an alias is provided for the fully qualified name.
     *
     * @return bool true if an alias was provided, false otherwise.
     */
    public function hasAlias()
    {
        return ($this->getAlias() !== '');
    }
    
    /**
     * Returns if present the alias for the fully qualified name.
     *
     * @return string an alias or empty string.
     */
    public function getAlias()
    {
        return (is_string($this->alias)) ? $this->alias : '';
    }
    
    /**
     * Returns the fully qualified name.
     *
     * @return string a fully qualified name.
     */
    public function getNamespace()
    {
        return (is_string($this->namespace)) ? $this->namespace : '';
    }
    
    /**
     * Returns the last part of a fully qualified name.
     *
     * If a {@link UseDescriptor} object represents the following fully qualified name 
     * such as 'My\Namespace\FooBar' then calling this method would return 'FooBar'.
     *
     * @return string the last part of a fully qualified name.
     */
    public function getName()
    {
        
    }
}
