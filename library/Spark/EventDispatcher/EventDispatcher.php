<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Spark\EventDispatcher;

use Spark\Collection\PriorityList;
use Spark\Common\Comparator\ComparableComparator;

/**
 * The EventDispatcher is resposible for dispatching events to one or more listeners.
 * This class also supports event filters which can be used to modify an event before
 * the actual event is dispatched or it can prevent an event from being dispatchted. 
 *
 * @author Chris Harris
 * @version 1.0.0
 * @since 1.0.0
 */
class EventDispatcher implements EventDispatcherInterface
{
    /**
     * A collection of event filters.
     *
     * @var array
     */
    private $filters = array();
    
    /**
     * A collection of event handler.
     *
     * @var array
     */
    private $handlers = array();
     
    /**
     * {inheritDoc}
     */
    public function addEventFilter($eventName, $eventFilter, $priority = 0)
    {
        if (!$this->hasEventFilters($eventName)) {
            $filters = new PriorityList();
            $filters->setComparator(new ComparableComparator());
            
            $this->filters[$eventName] = $filters;
        }

        $this->filters[$eventName]->add(new EventItem($eventFilter, $priority));
    }
    
    /**
     * {inheritDoc}
     */
    public function removeEventFilter($eventName, $eventFilter)
    {        
        if ($this->hasEventFilters($eventName)) {
            $filters = $this->filters[$eventName];
            
            $items = $filters->toArray();
            foreach ($items as $items) {
                if ($item->getListener() === $eventFilter) {
                    $filters->remove($item);
                }
            }
        }
    }
    
    /**
     * {inheritDoc}
     */
    public function getEventFilters($eventName = null)
    {        
        $eventFilters = array();
        if ($this->hasEventFilters($eventName)) {
            $items = $this->filters[$eventName];
            foreach ($items as $item) {
                $eventFilters[] = $item->getListener();
            }
        }
        
        return $eventFilters;
    }
    
    /**
     * {inheritDoc}
     */
    public function hasEventFilters($eventName)
    {
        return (isset($this->filters[$eventName]) && count($this->filters[$eventName]) > 0);
    }
     
    /**
     * {inheritDoc}
     */
    public function addEventHandler($eventName, $eventHandler, $priority = 0)
    {
        if (!$this->hasEventHandlers($eventName)) {
            $handlers = new PriorityList();
            $handlers->setComparator(new ComparableComparator());
            
            $this->handlers[$eventName] = $handlers;
        }
        
        $this->handlers[$eventName]->add(new EventItem($eventHandler, $priority));
    }
    
    /**
     * {inheritDoc}
     */
    public function removeEventHandler($eventName, $eventHandler)
    {
        if ($this->hasEventHandlers($eventName)) {
            $handlers = $this->handlers[$eventName];
            
            $items = $handlers->toArray();
            foreach ($items as $items) {
                if ($item->getListener() === $eventHandler) {
                    $handlers->remove($item);
                }
            }
        }
    }
    
    /**
     * {inheritDoc}
     */
    public function getEventHandlers($eventName = null)
    {
        $eventHandlers = array();
        if ($this->hasEventHandlers($eventName)) {
            $items = $this->handlers[$eventName];
            foreach ($items as $item) {
                $eventHandlers[] = $item->getListener();
            }
        }
        
        return $eventHandlers;
    }
    
    /**
     * {inheritDoc}
     */
    public function hasEventHandlers($eventName)
    {
        return (isset($this->handlers[$eventName]) && count($this->handlers[$eventName]) > 0);
    }
    
    /**
     * {inheritDoc}
     */
    public function dispatch($eventName, Event $event = null)
    {
        $event = ($event !== null) ? $event : new Event();
        $event->setDispatcher($this);
        $event->setName($eventName);
        
        // dispatch capturing phase.
        $event = $this->dispatchCapturingEvent($eventName, $event);
        if ($event->isConsumed()) {
            return null;
        }
        
        // dispatch bubbling phase.
        $event = $this->dispatchBubblingEvent($eventName, $event);
        if ($event->isConsumed()) {
            return null;
        }
        
        return $event;
    }
    
    /**
     * Dispatch event to all event filters, this is also known as the event capturing phase.
     * 
     * An event filter can give addtional information to the event or consume the event which
     * will prevent additional filters or event handlers from receiving the event.
     *
     * @param string $eventName the name of the event to dispatch.
     * @param Event|null the event to dispatch.
     * @return Event the event that was dispatched.
     */
    private function dispatchCapturingEvent($eventName, Event $event)
    {
        $filters = $this->getEventFilters($eventName);
        if (!empty($filters)) {
            foreach ($filters as $filter) {
                call_user_func($filter, $event);
                if ($event->isConsumed()) {
                    break;
                }
            }
        }
        return $event;
    }
    
    /**
     * Dispatch event to all event handlers, this is also known as the event bubbling phase.
     * 
     * An event handler can consume the event at any point which will prevent additional handlers
     * from receiving the event.
     *
     * @param string $eventName the name of the event to dispatch.
     * @param Event|null the event to dispatch.
     * @return Event the event that was dispatched.
     */
    private function dispatchBubblingEvent($eventName, Event $event)
    {
        $handlers = $this->getEventHandlers($eventName);
        if (!empty($handlers)) {
            foreach ($handlers as $handler) {
                call_user_func($handler, $event); 
                if ($event->isConsumed()) {
                    break;
                }
            }
        }
        return $event;
    }
}
