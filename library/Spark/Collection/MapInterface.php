<?php
/**
 * Copyright (c) 2016, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2016 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Spark\Collection;

use Countable;
use IteratorAggregate;

/**
 * An object that maps keys to values. A key can only be mapped to a single value, and it's for that reason impossible
 * to store duplicate keys. A map is similar to a dictionary or lookup table available in other programming languages.
 * 
 * @author Chris Harris <c.harris@hotmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
interface MapInterface extends Countable, IteratorAggregate
{
    /**
     * Associates the specified value with the specified key in this map. Any previously associated value for the specified
     * key is replaced by the new value.
     *
     * @param string|int $key the key that will be mapped to the specified value.
     * @param mixed $value the value to associate with the specified key.
     * @return mixed the previously associated value with the key, or null if there was no mapping.
     */
    public function add($key, $value);
    
    /**
     * Add a collection containing key-value pairs into this map.
     *
     * @param array|Traversable $items a collection containing key-value pairs.
     * @throws InvalidArgumentException if the specified argument is not an array or Traversable object.
     */
    public function addAll($items);
    
    /**
     * Returns the value associated with the specified key, or the default value if this map contains no mapping for the specified key.
     *
     * @param string|int $key the key whose value will be returned.
     * @param mixed $default the value to return if no mapping exists for the specified key.
     * @return mixed the value associated with the specified key, or the $default value if no mapping exists for the key.
     */
    public function get($key, $default = null);
    
    /**
     * Returns true if this map contains the specified key. More formally returns true only if this map
     * contains a key $k such that ($k === $key).
     *
     * @param string|int $key the key whose presence will be tested.
     * @return bool true if this map contains the specified key, false otherwise.
     */
    public function containsKey($key);
    
    /**
     * Returns true if this map contains the specified value. More formally returns true only if this map
     * contains a value $v such that ($v === $value).
     *
     * @param mixed $value the value whose presence will be tested.
     * @return bool true if this map contains the specified value, false otherwise.
     */
    public function containsValue($value);
    
    /**
     * Removes if present the mapping for a key from this map. More formally removes a mapping from this map where
     * key $k will be equal to ($k === $key).
     *
     * @param string|int $key the key whose mapping will be removed.
     * @return mixed the value that was previously associated with the mapping, or null if no mapping exists for the key.
     */
    public function remove($key);
    
    /**
     * Replaces if present the value associated with the specified key.
     *
     * @param string|int $key the key whose value will be replaced.
     * @param mixed $value the value to associate with the specified key.
     * @return mixed the value that was previously associated with the specified key, or null if there was no mapping for the key.
     */
    public function replace($key, $value);
    
    /**
     * Removes all of the mappings from this map. The map will be empty after this call returns.
     */
    public function clear();
    
    /**
     * Returns true if this map is considered to be empty.
     *
     * @return bool true is this map contains no elements, false otherwise.
     */ 
    public function isEmpty();
    
    /**
     * Returns a set of keys contained in this map.
     *
     * @return SetInterface a set of keys contained in this map.
     */
    public function keys();
    
    /**
     * Returns a numeric array of values contained in this map.
     *
     * @return ListInterface a list of values contained in this map.
     */
    public function values();
    
    /**
     * Returns a new map with items that match the specified predicate.
     *
     * @param callable $predicate the predicate to determine which items should be included.
     * @return MapInterface a new map with items that meet the criteria of the specified predicate.
     */
    public function filter($predicate);
}
