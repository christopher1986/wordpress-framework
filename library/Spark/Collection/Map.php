<?php
/**
 * Copyright (c) 2016, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2016 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Spark\Collection;

use ArrayAccess;
use ArrayIterator;

/**
 * This class implements the {@see MapInterface}, and is backed by a native array.
 *
 * @author Chris Harris <c.harris@hotmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
class Map implements MapInterface, ArrayAccess
{
    /**
     * A native array to hold the elements.
     *
     * @var array
     */
    private $items = array();

    /**
     * Construct a new Map.
     *
     * @param array|Traversable $items (optional) a collection of key-value pairs to add to this map.
     */
    public function __construct($items = null)
    {
        if ($items !== null) {
            $this->addAll($items);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public function add($key, $value)
    {
        $oldValue = $this->get($key);        
        $this->items[$key] = $value;
        
        return $oldValue;
    }
    
    /**
     * {@inheritDoc}
     */
    public function addAll($items)
    {
        if ($items instanceof \Traversable) {
            $items = iterator_to_array($items);
        }
    
        if (!is_array($items)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an array or Traversable object; received "%s"',
                __METHOD__,
                (is_object($items) ? get_class($items) : gettype($items))
            ));
        }
        
        foreach ($items as $key => $value) {
            $this->items[$key] = $value;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public function get($key, $default = null)
    {
        return (array_key_exists($key, $this->items)) ? $this->items[$key] : $default;
    }

    /**
     * {@inheritDoc}
     */
    public function containsKey($key)
    {
        return (array_key_exists($key, $this->items));
    }
    
    /**
     * {@inheritDoc}
     */
    public function containsValue($value)
    {    
        return (in_array($value, $this->items, true));
    }
    
    /**
     * {@inheritDoc}
     */
    public function remove($key)
    {
        $oldValue = $this->get($key);
        if (array_key_exists($key, $this->items)) {
            unset($this->items[$key]);
        }
        
        return $oldValue;
    }
    
    /**
     * {@inheritDoc}
     */
    public function replace($key, $value)
    {
        $oldValue = $this->get($key);
        if (array_key_exists($key, $this->items)) {
            $this->add($key, $value);
        }
        
        return $oldValue;
    }
    
    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->items = array();
    }
    
    /**
     * Returns the number of items contained by this map.
     *
     * @return int the number of items contained by this map.
     */
    public function count()
    {
        return (count($this->items));
    }
    
    /**
     * {@inheritDoc}
     */
    public function isEmpty()
    {
        return ($this->count() === 0);
    }
    
    /**
     * {@inheritDoc}
     */
    public function keys()
    {
        return new HashSet(array_keys($this->items));
    }
    
    /**
     * {@inheritDoc}
     */
    public function values()
    {
        return new ArrayList(array_values($this->items));
    }
    
    /**
     * {@inheritDoc}
     */
    public function filter($predicate)
    {
        $items = array();

        foreach ($this as $key => $value) {
            if ($predicate($value, $key)) {
                $items[$key] = $value;
            }
        }
        
        return new self($items);
    }

    /**
     * Returns an external iterator over the items contained by this map.
     *
     * @return Iterator an external iterator.
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }
    
    /**
     * Returns true if this collection contains the specified key. More formally returns true only if this collection
     * contains a key $k such that ($k === $key).
     *
     * @param mixed $key the key whose presence will be tested.
     * @return bool true if this collection contains the specified key, false otherwise.
     */
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->items);
    }
    
    /**
     * Returns the value associated with the specified key, or null if this map contains no mapping for the specified key.
     *
     * @param mixed $key the key whose value will be returned.
     * @param mixed $default the value to return if no mapping exists for the specified key.
     * @return mixed the value associated with the specified, or null if no mapping exists for the key.
     */
    public function offsetGet($key)
    {
        return $this->get($key);
    }
    
    /**
     * Associates the specified value with the specified key in this map. Any previously associated value for the specified
     * key is replaced by the new value.
     *
     * @param mixed $key the key that will be mapped to the specified value.
     * @param mixed $value the value to associate with the specified key.
     */
    public function offsetSet($key, $value)
    {
        $this->add($key, $value);
    }
    
    /**
     * Removes if present the mapping for a key from this map. More formally removes a mapping from this map where
     * key $k will be equal to ($k === $key).
     *
     * @param mixed $key the key whose mapping will be removed.
     */
    public function offsetUnset($key)
    {
        $this->remove($key);
    }
}
