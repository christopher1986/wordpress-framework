<?php
/**
 * Copyright (c) 2016, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2016 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Spark\Collection;

use Countable;
use IteratorAggregate;

/**
 * A collection (also known as a sequence) whose elements are ordered using a comparator.
 *
 * It's impossible to retrieve or insert elements using an index since the position of elements within 
 * a priority list are likely to change when new elements are added or existing ones are removed.
 *
 * @author Chris Harris <c.harris@hotmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
interface PriorityListInterface extends Countable, IteratorAggregate
{
    /**
     * Add the specified element to this list.
     *
     * @param mixed $element the element to add to this list.
     * @return bool true if this list did not already contain the specified element.
     */
    public function add($element);
    
    /**
     * Add to this list all of the elements that are contained in the specified collection.
     *
     * @param array|\Traversable $elements collection containing elements to add to this list.
     * @return bool true if the list has changed, false otherwise.
     * @throws InvalidArgumentException if the given argument is not an array or Traversable object.
     */
    public function addAll($elements);
    
    /**
     * Removes all elements from this list. The list will be empty after this call returns.
     */
    public function clear();
    
    /**
     * Returns true if this list contains the specified element. More formally returns true only if this list
     * contains an element $e such that ($e === $element).
     *
     * @param mixed $element the element whose presence will be tested.
     * @return bool true if this list contains the specified element, false otherwise.
     */
    public function contains($element);
    
    /**
     * Returns true if this list contains all the elements contained by the specified collection.
     *
     * @param array|\Traversable $elements collection of elements whose presence will be tested.
     * @return bool true if this list contains all elements in the specified collection, false otherwise.
     */
    public function containsAll($elements);
    
    /**
     * Returns true if this list is considered to be empty.
     *
     * @return bool true is this list contains no elements, false otherwise.
     */
    public function isEmpty();
    
    /**
     * Removes if present the specified element from this list. More formally removes an element $e such 
     * that ($e === $element), if this list contains such an element.
     *
     * @param mixed $element the element to remove from this list.
     * @return boolean true if this list contained the specified element, false otherwise.
     */
    public function remove($element);
    
    /**
     * Removes from this list all of the elements that are contained in the specified collection.
     *
     * @param array|\Traversable $elements collection containing elements to remove from this list.
     * @return bool true if the list has changed, false otherwise.
     * @throws InvalidArgumentException if the given argument is not an array or Traversable object.
     */
    public function removeAll($elements);
    
    /**
     * Retains only the elements in this list that are contained in the specified collection. In other words,
     * remove from this list all of it's elements that are not contained in the specified collection.
     *
     * @param array|\Traversable $elements collection containing element to be retained in this list.
     * @return bool true if the list has changed, false otherwise.
     * @throws InvalidArgumentException if the given argument is not an array or Traversable object.
     */
    public function retainAll($elements);
    
    /**
     * Returns a new list with elements that match the specified predicate.
     *
     * @param callable $predicate the predicate to determine which elements should be included.
     * @return PriorityListInterface a new list with elements that meet the criteria of the specified predicate.
     */
    public function filter($predicate);
    
    /**
     * Returns an array containing all of the elements in this list.
     *
     * @return array an array containing all the elements in this list.
     */
    public function toArray();
}
