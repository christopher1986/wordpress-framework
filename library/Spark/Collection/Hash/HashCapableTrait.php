<?php
/**
 * Copyright (c) 2016, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2016 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Spark\Collection\Hash;

use ReflectionClass;

/**
 * The HashCapableTrait provides a basic implementation of the {@link HashCapableInterface}.
 *
 * @author Chris Harris <c.harris@hotmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
trait HashCapableTrait
{
    /**
     * Allows the trait to introspect a class.
     *
     * @var ReflectionClass
     */
    private $refClass;

    /**
     * Returns a unique identifier for this object.
     * 
     * The algorithm used to generate the hash is a balance between practicality and performance.
     * In some rare situations this may lead to two identical hashes for different objects. 
     *
     * @return string a unique identifier.
     */
    public function getHashCode()
    {        
        if ($this->refClass === null) {
            $this->refClass = new ReflectionClass($this);
        }
    
        $hash = '';
    
        $props = $this->refClass->getProperties();
        foreach ($props as $prop) {
            $prop->setaccessible(true);
            $hash .= $this->computeHash($prop->getValue($this));
        }
    
        return md5($hash);
    }
    
    /**
     * Computes the hash for the specified item.
     *
     * @param mixed $item the item whose hash to compute.
     * @return string the computed hash for the specified item, or '0' on failure.
     */
    private function computeHash($item)
    {
        if (is_object($item)) {
            if ($item instanceof HashCapableInterface) {
                return $item->hashCode();
            }
            
            return sprintf('obj_%s', md5(serialize($item)));
        } else if (is_array($item)) {
            return sprintf('arr_%s', md5(serialize($item)));
        } else if (is_resource($item)) {
            return sprintf('res_%s', $item);
        } else if (is_scalar($item)) {
            return sprintf('str_%s', $item);
        }
        
        return '0';
    }
}
