<?php
/**
 * Copyright (c) 2016, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2016 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Spark\Collection;

use Spark\Common\Comparator\ComparableComparator;
use Spark\Common\Comparator\CompositeComparator;
use Spark\Common\Comparator\NumericComparator;
use Spark\Common\Comparator\StringComparator;
use Spark\Util\Arrays;

use Spark\Collection\Iterator\ListIterator;

class PriorityList implements PriorityListInterface
{
    /**
     * the underlying list.
     *
     * @var array
     */
    private $elements = null;

    /**
     * A comparator used to order the list.
     *
     * @var ComparatorInterface
     */
    private $comparator = null;
    
    /**
     * A flag to determine if the list is stored.
     *
     * @var bool
     */
    private $sorted = true;
    
    /**
     * Construct a new PriorityList.
     *
     * @param array|Traversable $elements (optional) a collection of elements to add to this list.
     */
    public function __construct($elements = null)
    {
        $this->elements = new ArrayList($elements);
        $this->sorted   = $this->elements->isEmpty();
    }
    
    /**
     * {@inheritDoc}
     */
    public function add($element)
    {    
        $modified     = $this->elements->add($element);
        $this->sorted = ($this->sorted && $modified === false);
        
        return $modified;
    }
    
    /**
     * {@inheritDoc}
     */
    public function addAll($elements)
    {
        $modified     = $this->elements->add($element);
        $this->sorted = ($this->sorted && $modified === false);
        
        return $modified;
    }
    
    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->elements->clear();
        $this->sorted = true;
    }

    /**
     * {@inheritDoc}
     */
    public function contains($element)
    {
        return $this->elements->contains($element);
    }
    
    /**
     * {@inheritDoc}
     */
    public function containsAll($elements)
    {
        return $this->elements->containsAll($elements);
    }
    
    /**
     * {@inheritDoc}
     */
    public function isEmpty()
    {
        return $this->elements->isEmpty();
    }
    
    /**
     * {@inheritDoc}
     */
    public function remove($element)
    {
        $modified     = $this->remove($element);
        $this->sorted = ($this->sorted && $modified === false);
        
        return $modified;
    }
    
    /**
     * {@inheritDoc}
     */
    public function removeAll($elements)
    {
        $modified     = $this->elements->removeAll($elements);
        $this->sorted = ($this->sorted && $modified === false);
        
        return $modified;
    }
    
    /**
     * {@inheritDoc}
     */ 
    public function retainAll($elements)
    {
        $modified     = $this->retainAll($elements);
        $this->sorted = ($this->sorted && $modified === false);
        
        return $modified;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
        $this->ensureSorted();    
        return $this->elements->toArray();
    }
    
    /**
     * {@inheritDoc}
     */
    public function filter($predicate)
    {
        $list = new self($this->elements->filter($predicate));
        $list->sorted = $this->sorted;
        
        return $list;
    }
    
    /**
     * Returns an external iterator over the elements in this list.
     *
     * @return ListIterator an iterator over the elements in this list.
     */    
    public function getIterator()
    {
        $this->ensureSorted(); 
        return new ListIterator($this->elements);
    }
    
    /**
     * Returns the number of elements contained by this list.
     *
     * @return int the number of elements contained by this list.
     */
    public function count()
    {
        return count($this->elements);
    }
    
    /**
     * Set a {@link ComparatorInterface} instance to order the elements contained within this list.
     *
     */
    public function setComparator(ComparatorInterface $comparator)
    {
        $this->comparator = $comparator;
    }
    
    /**
     * Returns the {@link ComparatorInterface} instance used to order elements contained within this list.
     *
     * @return ComparatorInterface a comparator used to order the elements contained within this list.
     */
    public function getComparator()
    {        
        if ($this->comparator === null) {
            $this->comparator = new CompositeComparator(array(
                new ComparableComparator(),
                new NumericComparator(),
                new StringComparator()
            ));
        }
    
        return $this->comparator;
    }
    
    /** 
     * Ensure that the underlying list is sorted.
     */
    private function ensureSorted()
    {
        if (!$this->sorted) {
            $elements = $this->toArray();
            Arrays::sort($elements, $this->getComparator());
            
            $this->elements = new ArrayList($elements);
            $this->sorted   = true;
        }
    }
}
