<?php

namespace Spark\Routing\Route;

use Spark\Http\RequestInterface;

/**
 * This interface allows a route to determine whether it matches a HTTP request.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
interface MatchableInterface
{
    /**
     * Tests whether the route matches with the given request.
     *
     * @param RequestInterface $request the HTTP request to match against.
     * @return bool true if the route matches with the request, false otherwise.
     */
    public function match(RequestInterface $request);
}
